# Inverse Kinematics Controller
This repo is the middle layer of LLEAP's exoskeleton's control system, responsible for using inverse kinematics (among other tools) to determine joint parameters (angles) along a trajectory given only target toes poses. This repo (and any packages therein) should work with the simulation elsewhere.
<br>*NOTE*: As of 2/2/22, cleanup probably needs to be done organizationally for better architectural practices, but right now it's fine.
